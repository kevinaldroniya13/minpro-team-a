package com.teama.minpro.teama.controller;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.teama.minpro.teama.model.Biodata;
import com.teama.minpro.teama.model.Customer;
import com.teama.minpro.teama.model.User;
import com.teama.minpro.teama.repository.BiodataRepository;
import com.teama.minpro.teama.repository.CustomerRepository;
import com.teama.minpro.teama.repository.UserRepository;

@RestController
@RequestMapping("/api/profilshared/")
public class ApiProfilSharedController {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private BiodataRepository biodataRepository;

	@GetMapping("getprofilebyuserid/{userId}")
	public ResponseEntity<Object> getProfileByUserId(@PathVariable("userId") Long id) {
		try {
			Optional<User> user = this.userRepository.findById(id);
			return new ResponseEntity<>(user, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("getcustomerbybioid/{bioId}")
	public ResponseEntity<Object> getCustomerById(@PathVariable("bioId") Long id) {
		try {
			Customer biodata = this.customerRepository.findByBioId(id);
			return new ResponseEntity<>(biodata, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("getbiodatabyid/{id}")
	public ResponseEntity<Object> getBiodataById(@PathVariable("id") Long id) {
		try {
			Optional<Biodata> biodata = this.biodataRepository.findById(id);
			return new ResponseEntity<>(biodata, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	public static String uploadDirectory = System.getProperty("user.dir") + "/src/main/webapp/img";

	@PutMapping("ubahprofilepic")
	public ResponseEntity<Object> ubahProfilePic(@RequestParam("file") MultipartFile file,
			@RequestParam("id") Long id) {

		LocalDateTime datetime = LocalDateTime.now();
		DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("yyyyMMdd+mmss");
		// UUID randomUUID = UUID.randomUUID();
		// String filename = randomUUID.toString() + "-"
		// +vmcategory.file.getOriginalFilename();
		String filename = datetime.format(formatDate) + "-" + file.getOriginalFilename();

		Path fileNameAndPath = Paths.get(uploadDirectory, filename);

		try {
			Files.write(fileNameAndPath, file.getBytes());
			Biodata bio = this.biodataRepository.findById(id).orElse(null);
			String replacedImage = bio.getPathImage();
			if (bio.getPathImage() != null) {
				Path fileNameAndPathDel = Paths.get(uploadDirectory, replacedImage);
				try {
					Files.delete(fileNameAndPathDel);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} 

			bio.setPathImage("/img/" + filename);
			bio.setModifiedBy((long) 1);
			bio.setModifiedOn(LocalDateTime.now());
			this.biodataRepository.save(bio);
			return new ResponseEntity<>(bio, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

}

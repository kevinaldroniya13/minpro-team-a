package com.teama.minpro.teama.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.teama.minpro.teama.model.CustomerChat;

public interface CustomerChatRepository extends JpaRepository<CustomerChat, Long>{
	@Query(value="SELECT * FROM t_customer_chat WHERE customer_id = ?1 AND is_delete = false",nativeQuery=true)
	List<CustomerChat>findByCustomerId(Long id);
}

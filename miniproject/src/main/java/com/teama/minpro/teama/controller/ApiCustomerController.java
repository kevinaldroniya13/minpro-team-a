package com.teama.minpro.teama.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.teama.minpro.teama.model.Appointment;
import com.teama.minpro.teama.model.CustomerChat;
import com.teama.minpro.teama.model.CustomerMember;
import com.teama.minpro.teama.repository.AppointmentRepository;
import com.teama.minpro.teama.repository.CustomerChatRepository;
import com.teama.minpro.teama.repository.CustomerMemberRepository;

@RestController
@RequestMapping("/api/customer/")
public class ApiCustomerController {
	
	@Autowired
	private CustomerMemberRepository customerMemberRepository;
	
	@Autowired
	private CustomerChatRepository customerChatRepository;
	
	@Autowired
	private AppointmentRepository appointmentRepository;
	
	@GetMapping("getcustomermemberbyparentbioid/{id}")
	public ResponseEntity<List<CustomerMember>> getCustomerMemberByBioId(@PathVariable("id")Long id){
		try {
			List<CustomerMember> customerMemberData = this.customerMemberRepository.findByParentBioId(id);
			return new ResponseEntity<>(customerMemberData, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("getchatbycustomerid/{customerId}")
	public ResponseEntity<List<CustomerChat>> getChatByCustomerId(@PathVariable("customerId")Long id){
		try {
			List<CustomerChat> customerChatData = this.customerChatRepository.findByCustomerId(id);		
			return new ResponseEntity<>(customerChatData, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("getappointmentbycustomerid/{customerId}")
	public ResponseEntity<List<Appointment>> getAppointmentByCustomerId(@PathVariable("customerId")Long id){
		try {
			List<Appointment> customerAppointmentData = this.appointmentRepository.findAppointmentByCustomerId(id);		
			return new ResponseEntity<>(customerAppointmentData, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	/*
	@GetMapping("getcustomermember/paging")
	public ResponseEntity<Map<String, Object>>getCustomerMemberPaging(@RequestParam(defaultValue = "0") int currentPage,
			@RequestParam(defaultValue = "5") int size, @RequestParam("keyword") String keyword,
			@RequestParam("sortType") String sortType, @RequestParam("parentBioId")Long PBioId){
		try {
			List<CustomerMember> customerMember = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(currentPage, size);
			if (sortType.equals("ASC")) {
				pages = this.customerMemberRepository.findCustomerMemberASC(keyword, false, PBioId, pagingSort);
			}else {
				pages = this.customerMemberRepository.findCustomerMemberDESC(keyword, false, PBioId, pagingSort);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	*/
}

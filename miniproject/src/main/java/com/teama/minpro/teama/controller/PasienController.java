package com.teama.minpro.teama.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/pasien/")
public class PasienController {
	

	@GetMapping("profilshared/{userId}")
	public ModelAndView profilshared() {
		ModelAndView view = new ModelAndView("pasien/profilshared.html");
		return view;
	}
	
	@GetMapping("t_pasien/{userId}")
	public ModelAndView pasienmember() {
		ModelAndView view = new ModelAndView("pasien/t_pasien.html");
		return view;
	}

}

package com.teama.minpro.teama.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.teama.minpro.teama.model.CustomerMember;

public interface CustomerMemberRepository extends JpaRepository<CustomerMember, Long>{
	
	@Query(value="SELECT * FROM m_customer_member WHERE parent_biodata_id = ?1 AND is_delete = false ORDER BY id ASC", nativeQuery=true)
	List<CustomerMember> findByParentBioId(Long id);
	
	@Query(value="SELECT * FROM m_customer_member M JOIN m_customer C ON M.customer_id = C.id JOIN m_biodata B ON JOIN m_biodata B ON C.biodata_id = B.id = B.id WHERE LOWER(B.fullname) LIKE LOWER(CONCAT('%',?1,'%')) AND M.is_delete =? 2 AND M.parent_biodata_id =?3 ORDER BY B.fullname ASC", nativeQuery=true)
	Page<CustomerMember> findCustomerMemberASC(String keyword, Boolean isDelete, Long PbioId, Pageable page);
	
}

package com.teama.minpro.teama.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.teama.minpro.teama.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

	@Query(value = "SELECT * FROM m_customer WHERE biodata_id =?1 AND is_delete = false ORDER BY id ASC", nativeQuery = true)
	Customer findByBioId(Long id);

}

package com.teama.minpro.teama.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.teama.minpro.teama.model.BloodGroup;

public interface BloodGroupRepository extends JpaRepository<BloodGroup, Long>{

}

package com.teama.minpro.teama.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.teama.minpro.teama.model.User;
import com.teama.minpro.teama.model.VMBiodata;
import com.teama.minpro.teama.repository.BiodataRepository;
import com.teama.minpro.teama.repository.UserRepository;

@RestController
@RequestMapping("/api")
public class ApiProfileDoctorController {
	private String imgPath = null;
	
	
	@Autowired
	private BiodataRepository biodataRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@GetMapping("/profile/doctor/{id}")
	public ResponseEntity<Object> getDoctorData(@PathVariable("id") Long id ,VMBiodata vmBiodata){
		try {
			Optional<User> data = userRepo.findById(id);
			return new ResponseEntity<>(data, HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	
}

package com.teama.minpro.teama.model;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="t_appointment")
public class Appointment {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	public Long id;
	
	@Column(name="customer_id")
	public Long customerId;
	
	@ManyToOne
	@JoinColumn(name="customer_id",insertable=false, updatable=false)
	public Customer customer;
	
	@Column(name="doctor_office_id")
	public Long doctorOfficeId;
	
	/*
	@ManyToOne
	@JoinColumn(name="doctor_office_id", insertable=false, updatable=false)
	public DoctorOffice doctorOffice;
	*/
	
	@Column(name="doctor_office_schedule_id")
	public Long doctorOfficeScheduleId;
	
	/*
	@ManyToOne
	@JoinColumn(name="doctor_office_schedule_id", insertable=false, updatable=false)
	public DoctorOfficeSchedule doctorOfficeSchedule;
	*/
	
	@Column(name="doctor_office_treatment_id")
	public Long doctorOfficeTreatmentId;
	
	/*
	@ManyToOne
	@JoinColumn(name="doctor_office_treatment_id", insertable=false, updatable=false)
	public DoctorOfficeTreatment doctorOfficeTreatment;
	*/
	
	@Column(name="appointment_date")
	public LocalDateTime appointmentDate;
	
	@Column(name="created_by")
	public Long createdBy;
	
	@Column(name="created_on")
	public Date createdOn;
	
	@Column(name="modified_by")
	public Long modifiedBy;
	
	@Column(name="modified_on")
	public Date modifiedOn;
	
	@Column(name="deleted_by")
	public Long deletedBy;
	
	@Column(name="deleted_on")
	public Date deletedOn;
	
	@Column(name="is_delete")
	public Boolean isDelete;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getDoctorOfficeId() {
		return doctorOfficeId;
	}

	public void setDoctorOfficeId(Long doctorOfficeId) {
		this.doctorOfficeId = doctorOfficeId;
	}

	public Long getDoctorOfficeScheduleId() {
		return doctorOfficeScheduleId;
	}

	public void setDoctorOfficeScheduleId(Long doctorOfficeScheduleId) {
		this.doctorOfficeScheduleId = doctorOfficeScheduleId;
	}

	public Long getDoctorOfficeTreatmentId() {
		return doctorOfficeTreatmentId;
	}

	public void setDoctorOfficeTreatmentId(Long doctorOfficeTreatmentId) {
		this.doctorOfficeTreatmentId = doctorOfficeTreatmentId;
	}

	public LocalDateTime getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(LocalDateTime appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	

}

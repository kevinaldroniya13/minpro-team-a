package com.teama.minpro.teama.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.teama.minpro.teama.model.Appointment;

public interface AppointmentRepository extends JpaRepository<Appointment, Long>{
	@Query(value="SELECT * FROM t_appointment WHERE customer_id =?1 AND is_delete=false",nativeQuery=true)
	List<Appointment> findAppointmentByCustomerId(Long id);
}
